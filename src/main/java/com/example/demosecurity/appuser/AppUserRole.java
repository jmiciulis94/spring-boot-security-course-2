package com.example.demosecurity.appuser;

public enum AppUserRole {
    USER,
    ADMIN
}
