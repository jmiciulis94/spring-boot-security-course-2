package com.example.demosecurity.email;

public interface EmailSender {

    void send(String to, String email);
}
